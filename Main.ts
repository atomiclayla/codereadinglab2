// A Room value is exactly one of these four strings.
// It is impossible for a Room variable to contain any other string.

currentRoom: <number>(2);

//example usage map = createMap(1,2)
export function createMap(x:number, y:number): Room[][]
{
  return Room[x][y];
}

enum DoorState {
  Open,
  Closed,
  Locked,
}

abstract class Room {
  abstract handleCommand(cmd: String);
  key: boolean;
  keyDoorId: number;
  north: DoorState;
  south: DoorState;
  east: DoorState;
  west: DoorState;

  constructor(
      key: boolean,
      keyDoorId: number,
      north: DoorState,
      south: DoorState,
      east: DoorState,
      west: DoorState,
    ) {
    this.key = key;
    this.keyDoorId = keyDoorId;
    this.north = north;
    this.south = north;
    this.east = north;
    this.west = north;
  }

}

export function process_cmd(currRoom: Room, msg: string): string {
  let input: string | null = null;
  while (!input) {
    input = prompt(msg);
    if (!input || input == "") {
      console.error("Invalid input.");
    }

    switch (msg) {
      case "north":
        switch (north) {
          case "Open":
          //move north
          case "Closed":
            //err
          case "Locked":
            //locked
        }
      case "south":
        switch (south) {
          case "Open":
            //move south
          case "Closed":
            //err
          case "Locked":
            //locked
        }
      case "east":
        switch (east) {
          case "Open":
            //move east
          case "Closed":
            //err
          case "Locked":
            //locked
        }
      case "west":
        switch (west) {
          case "Open":
            //move west
          case "Closed":
            //err
          case "Locked":
            //locked
        }
      case "getkey":
        if(key==true):
          //get key
      default: 
        //err
    }
  }
  return input;
}

export function process_input(msg: string): string {
  let input: string | null = null;
  while (!input) {
    input = prompt(msg);
    if (!input || input == "") {
      console.error("Invalid input.");
    }
  }
  return input;
}

export function room_transition(currentRoom: [2], direction: string): [2] {
  //implement logic for traversal of map here!
  newRoom: [2];
  return newRoom;
}

///////////////////////////////////
/*type Room = { name: string; desc: string };
let Building: Array<Room> = [
  {
    name: "A",
    desc: "You are in an empty room. There are doors on the north and west walls of this room.",
  },
  {
    name: "B",
    desc: "You go through the west door. You are in a room with a table.",
  },
  {
    name: "C",
    desc: "You are in a bright room. There is a door on the south wall of this room and a window on the east wall.",
  },
  { name: "Exit", desc: "" },
];

export function play(): void {
  console.info(
    "Welcome to the text adventure! Open your browser's developer console to play."
  );

  let playerName: string = process_input("Please enter your name.");
  console.log(playerName);

  console.info("Hello, " + playerName + ".");
  console.info("You are in a building. Your goal is to exit this building.");

  let currentRoom: Room = Building[0];
  let hasKey: boolean = false;
  let windowOpen: boolean = false;

  console.info(currentRoom.desc);

  while (currentRoom.name != "Exit") {
    let command: string = process_input("Please enter a command.");
    console.log(command);

    switch (currentRoom.name) {
      case "A":
        switch (command) {
          case "west":
            currentRoom = room_transition(Building[1]);
            if (!hasKey) {
              console.info("On the table there is a key.");
            }
            console.info("There is a door on the east wall of this room.");
            break;
          case "north":
            if (hasKey) {
              console.info(
                "You unlock the north door with the key and go through the door."
              );
              currentRoom = room_transition(Building[2]);
            } else {
              console.error(
                "You try to open the north door, but it is locked."
              );
            }
            break;
          default:
            console.error("Unrecognized command.");
            break;
        }
        break;

      case "B":
        switch (command) {
          case "east":
            currentRoom = room_transition(Building[0]);
            break;
          case "take key":
            if (hasKey) {
              console.error("You already have the key.");
            } else {
              console.info("You take the key from the table.");
              hasKey = true;
            }
            break;
          default:
            console.error("Unrecognized command.");
            break;
        }
        break;

      case "C":
        switch (command) {
          case "south":
            currentRoom = room_transition(Building[0]);
            break;
          case "east":
            if (windowOpen) {
              currentRoom = Building[3];
              console.info("You step out from the open window.");
            } else {
              console.error("The window is closed.");
            }
            break;
          case "open window":
            if (windowOpen) {
              console.error("The window is already open.");
            } else {
              console.info("You open the window.");
              windowOpen = true;
            }
            break;
          default:
            console.error("Unrecognized command.");
            break;
        }
        break;
    }
  }

  console.info("You have exited the building. You win!");
  console.info("Congratulations, " + playerName + "!");
}



export function room_transition(new_room: Room): Room {
  console.info(new_room.desc);
  return new_room;
}*/
